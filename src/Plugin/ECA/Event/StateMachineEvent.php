<?php

namespace Drupal\eca_state_machine\Plugin\ECA\Event;

use Drupal\eca\Attributes\Token;
use Drupal\eca\Plugin\ECA\Event\EventBase;
use Drupal\state_machine\Event\WorkflowTransitionEvent;

/**
 * Plugin implementation of the ECA Events for state_machine.
 *
 * @EcaEvent(
 *   id = "state_machine",
 *   deriver = "Drupal\eca_state_machine\Plugin\ECA\Event\StateMachineEventDeriver",
 *   eca_version_introduced = "1.0.0"
 * )
 */
class StateMachineEvent extends EventBase {

  /**
   * {@inheritdoc}
   */
  public static function definitions(): array {
    $actions = [];

    foreach (['pre_transition', 'post_transition'] as $phase) {
      $actions['state_machine.' . $phase] = [
        'label' => 'State Machine: ' . $phase,
        'event_name' => 'state_machine.' . $phase,
        'event_class' => WorkflowTransitionEvent::class,
      ];
    }

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  #[Token(
    name: 'event',
    description: 'The event.',
    classes: [
      WorkflowTransitionEvent::class,
    ],
    properties: [
      new Token(
        name: 'entity',
        description: 'The entity.',
      ),
    ],
  )]
  protected function buildEventData(): array {
    $event = $this->event;
    $data = [];
    if ($event instanceof WorkflowTransitionEvent) {
      $data['entity'] = $event->getEntity();
    }
    $data += parent::buildEventData();
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  #[Token(
    name: 'entity',
    description: 'The entity.',
    classes: [
      WorkflowTransitionEvent::class,
    ],
  )]
  public function getData(string $key): mixed {
    $event = $this->event;
    if ($event instanceof WorkflowTransitionEvent) {
      $entity = $event->getEntity();
      if ($key === 'entity') {
        return $entity;
      }
    }
    return parent::getData($key);
  }

}
